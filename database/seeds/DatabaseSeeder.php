<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        if (Role::whereName('admin')->count() < 1) {
	    	$role = Role::create([
	    		'name' => 'admin',
	    		'label' => 'Admin',
	    	]);
	        $user = User::create([
	        	'name' => 'Admin',
	        	'email' => 'admin@admin.com',
	        	'password' => bcrypt('admin'),
	        ]);
			$user->assignRole('admin');
        }
    }
}
