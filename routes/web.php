<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['namespace' => 'Admin','prefix' => 'admin', 'middleware' => ['auth', 'roles','can:browse-admin'], 'roles' => 'admin'], function () {
	Route::get('/', 'AdminController@index');
	Route::resource('/roles', 'RolesController');
	Route::resource('/permissions', 'PermissionsController');
	Route::resource('/users', 'UsersController');
	Route::resource('/pages', 'PagesController');
	Route::resource('/activitylogs', 'ActivityLogsController')->only([
	    'index', 'show', 'destroy'
	]);
	Route::resource('/settings', 'SettingsController');\
	Route::get('/generator', ['as' => 'generator.get','uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
	Route::post('/generator', ['as' => 'generator.post','uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
});
