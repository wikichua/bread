<?php

namespace App\Providers;

use File;
use Illuminate\Support\ServiceProvider;

class LaravelAdminServiceProvider extends \Appzcoder\LaravelAdmin\LaravelAdminServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->publishes([
            __DIR__ . '/../publish/Middleware/' => app_path('Http/Middleware'),
        ]);

        $this->publishes([
            __DIR__ . '/../publish/migrations/' => database_path('migrations'),
        ]);

        $this->publishes([
            __DIR__ . '/../publish/Model/' => app_path(),
        ]);

        $this->publishes([
            __DIR__ . '/../publish/Controllers/' => app_path('Http/Controllers'),
        ]);

        $this->publishes([
            __DIR__ . '/../publish/resources/' => base_path('resources'),
        ]);

        $this->publishes([
            __DIR__ . '/../publish/crudgenerator.php' => config_path('crudgenerator.php'),
        ]);

        $this->publishes([
            __DIR__ . '/views' => base_path('resources/views/vendor/laravel-admin'),
        ], 'views');

        $this->loadViewsFrom(__DIR__ . '/views', 'laravel-admin');

        $menus = [];
        $menus = json_decode(json_encode(config('menus')));
        view()->share('laravelAdminMenus', $menus);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands(
            '\Appzcoder\LaravelAdmin\LaravelAdminCommand'
        );

        $this->app->bind('Setting', \Appzcoder\LaravelAdmin\Setting::class);
    }
}
